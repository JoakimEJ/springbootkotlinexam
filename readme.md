## Booktrading REST API
The Booktrading REST API lets us manage users, books and sales, with the intent of laying the foundation for a book trading application 
that can be used by students to sell and buy their curriculum books. The project is my submission for the exam in the
course *PG6100 Enterpriseprogramming 2*

Link to repository will be made public shortly after the exam is due:
* https://bitbucket.org/JoakimEJ/springbootkotlinexam/src/master/

## Tools used
The project is written in Kotlin on the Spring Boot framework. It is built and managed with Maven,
documented/front-ended(?) with Swagger and tested with junit and restAssured. 
For persistence, H2 embedded database is used. A dockerfile for the application is also provided.

## Running the application
If you downloaded this project as a .zip, unpack and navigate to unpacked folder. From here, the following approaches are available. 

From root of the unpacked folder, run the following commands - depending on how you want to start the application:
####1 Directly from the project
1. *mvn clean install -DskipTests*
2. *cd booktrading-rest/target*
3. *java -jar booktradingRest.jar*
4. Swagger UI is now accessible at *http://localhost:8080/booktradingrest/api/swagger-ui.html*
5. Ctrl + c in terminal to shutdown when done.

PS: After running *mvn clean install -DskipTests* the application can also be run directly from the IDE. The main method is in class BookTradeRestApplication.

####2 Run application in docker container
    For this to work on Docker for Windows (10, Pro), we have to expose daemon on TCP without TLS.
    To do this, go to Docker settings => General => and tick the box for 
    'Expose daemon on tcp://localhost:2375 without TLS'

Again from root of unpacked folder, run the following command: 

1. *mvn -DskipTests -DbuildDocker=true clean install*
2. *docker run -p 8080:8080 -t enterpriseexam/booktrading-rest*
3. Swagger UI is now accessible at *http://localhost:8080/booktradingrest/api/swagger-ui.html*
4. Ctrl + c in terminal to shutdown when done.
5. *docker ps*
6. If container still running: *docker container stop [Container Name]*

These commands are only tested in the Intellij-terminal and powershell for windows 10.

To run tests: *mvn clean verify*

## Requirements
Requirements 1 and 2 have been completed in this task with one exception:
I was not able to use JSON Merge Patch. I commented it out but left it in the project to expose the attempt. The attempt is in the class BookTradingRestApi at line 260.

Apart from spring.io, baeldung.com, docs.docker.com and stackoverflow, this project is heavily influenced by the curriculum repository:

* https://github.com/arcuri82/testing_security_development_enterprise_systems