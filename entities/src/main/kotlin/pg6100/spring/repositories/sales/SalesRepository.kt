package pg6100.spring.repositories.sales

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import pg6100.spring.repositories.book.BookEntity
import pg6100.spring.repositories.user.UserEntity
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
interface SalesRepository : CrudRepository<SalesEntity, Long>, SalesRepositoryCustom

@Transactional
interface SalesRepositoryCustom {

    fun createSale(salesPrice: Int, bookEntity: BookEntity, userEntity: UserEntity): Long

    fun updateSale(salesId: Long, salesPrice: Int, bookEntity: BookEntity, userEntity: UserEntity): Boolean

    fun updateSalePrice(salesId: Long, salesPrice: Int): Boolean
}

@Repository
@Transactional
open class SalesRepositoryImpl : SalesRepositoryCustom {

    @PersistenceContext
    private lateinit var em: EntityManager

    override fun createSale(salesPrice: Int, bookEntity: BookEntity, userEntity: UserEntity): Long {
        val entity = SalesEntity(salesPrice, bookEntity, userEntity)
        em.persist(entity)
        return entity.salesId!!
    }

    override fun updateSale(salesId: Long, salesPrice: Int, bookEntity: BookEntity, userEntity: UserEntity): Boolean {
        val sale = em.find(SalesEntity::class.java, salesId) ?: return false
        sale.salesPrice = salesPrice
        sale.book = bookEntity
        sale.user = userEntity
        return true
    }

    override fun updateSalePrice(salesId: Long, salesPrice: Int): Boolean {
        val sale = em.find(SalesEntity::class.java, salesId)
        sale.salesPrice = salesPrice
        return true
    }
}