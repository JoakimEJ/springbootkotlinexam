package pg6100.spring.repositories.sales

import com.fasterxml.jackson.annotation.JsonBackReference
import pg6100.spring.repositories.book.BookEntity
import pg6100.spring.repositories.user.UserEntity
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
class SalesEntity(

        @get:NotNull
        var salesPrice: Int,

        @get:ManyToOne()
        @get:NotNull
        @JsonBackReference(value = "book-movement")
        var book: BookEntity,

        @get:ManyToOne()
        @get:NotNull
        @JsonBackReference(value = "user-movement")
        var user: UserEntity,

        @get:Id @get:GeneratedValue
        var salesId: Long? = null
)