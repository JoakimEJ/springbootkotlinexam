package pg6100.spring.repositories.user

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
interface UserRepository : CrudRepository<UserEntity, Long>, UserRepositoryCustom

@Transactional
interface UserRepositoryCustom {

    fun createUser(userName: String): Long
}

@Repository
@Transactional
open class UserRepositoryImpl : UserRepositoryCustom {

    @PersistenceContext
    private lateinit var em: EntityManager

    override fun createUser(userName: String): Long {
        val entity = UserEntity(userName)
        em.persist(entity)
        return entity.userId!!
    }
}