package pg6100.spring.repositories.user

import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.validator.constraints.NotBlank
import pg6100.spring.repositories.sales.SalesEntity
import javax.persistence.*

@Entity
class UserEntity(

        @get:NotBlank
        var userName: String,

        @get:OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], orphanRemoval = true)
        @JsonManagedReference(value = "user-movement")
        var sales: MutableList<SalesEntity> = mutableListOf(),

        @get:Id @get:GeneratedValue
        var userId: Long? = null
)