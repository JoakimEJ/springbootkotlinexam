package pg6100.spring.repositories.book

import com.fasterxml.jackson.annotation.JsonManagedReference
import org.hibernate.validator.constraints.NotBlank
import pg6100.spring.repositories.sales.SalesEntity
import javax.persistence.*

@Entity
class BookEntity(

        @get:NotBlank
        var bookName: String,

        @get:OneToMany(mappedBy = "book", cascade = [CascadeType.ALL], orphanRemoval = true)
        @JsonManagedReference(value = "book-movement")
        var sales: MutableList<SalesEntity> = mutableListOf(),

        @get:Id @get:GeneratedValue
        var bookId: Long? = null
)