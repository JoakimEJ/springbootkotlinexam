package pg6100.spring.repositories.book

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
interface BookRepository : CrudRepository<BookEntity, Long>, BookRepositoryCustom

@Transactional
interface BookRepositoryCustom {

    fun createBook(bookName: String): Long

    fun update(bookId: Long, bookName: String): Boolean
}

@Repository
@Transactional
open class BookRepositoryImpl : BookRepositoryCustom {

    @PersistenceContext
    private lateinit var em: EntityManager

    override fun createBook(bookName: String): Long {
        val entity = BookEntity(bookName)
        em.persist(entity)
        return entity.bookId!!
    }

    override fun update(bookId: Long, bookName: String): Boolean {
        val book = em.find(BookEntity::class.java, bookId) ?: return false
        book.bookName = bookName
        return true
    }
}