package pg6100.spring.repositories

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import pg6100.spring.repositories.book.BookRepository
import pg6100.spring.repositories.user.UserRepository

@RunWith(SpringRunner::class)
@DataJpaTest
@Transactional(propagation = Propagation.NEVER)
class BookTest {

    @Autowired
    private lateinit var bookCrud: BookRepository

    @Autowired
    private lateinit var userCrud: UserRepository

    val NAME = "someBookName"

    @Before
    fun cleanDatabase() {
        bookCrud.deleteAll()
        userCrud.deleteAll()
    }

    @Test
    fun testInitialization() {
        assertNotNull(bookCrud)
        assertNotNull(userCrud)
    }

    @Test
    fun testCreate() {
        assertEquals(0, bookCrud.count())

        val id = bookCrud.createBook(NAME)

        assertEquals(1, bookCrud.count())
        assertEquals(id, bookCrud.findOne(id).bookId)
    }

    @Test
    fun testDelete() {
        assertEquals(0, bookCrud.count())

        val id = bookCrud.createBook(NAME)
        assertTrue(bookCrud.exists(id))
        assertTrue(bookCrud.findAll().any { n -> n.bookId == id })
        assertEquals(1, bookCrud.count())

        bookCrud.delete(id)

        assertFalse(bookCrud.exists(id))
        assertFalse(bookCrud.findAll().any { n -> n.bookId == id })
        assertEquals(0, bookCrud.count())
    }

    @Test
    fun testGet() {

        val id = bookCrud.createBook(NAME)
        val book = bookCrud.findOne(id)

        assertEquals(NAME, book.bookName)
    }

    @Test
    fun testFindAll() {
        assertEquals(0, bookCrud.findAll().count())
        bookCrud.createBook(NAME)
        bookCrud.createBook(NAME)
        bookCrud.createBook(NAME)

        assertEquals(3, bookCrud.findAll().count())
    }

}
@SpringBootApplication
class TestApplication