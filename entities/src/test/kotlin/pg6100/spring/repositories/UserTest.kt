package pg6100.spring.repositories

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import pg6100.spring.repositories.book.BookRepository
import pg6100.spring.repositories.user.UserRepository

@RunWith(SpringRunner::class)
@DataJpaTest
@Transactional(propagation = Propagation.NEVER)
class UserTest {

    @Autowired
    private lateinit var bookCrud: BookRepository

    @Autowired
    private lateinit var userCrud: UserRepository

    val USERNAME = "someName"

    @Before
    fun cleanDatabase() {
        bookCrud.deleteAll()
        userCrud.deleteAll()
    }

    @Test
    fun testInitialization() {
        Assert.assertNotNull(bookCrud)
        Assert.assertNotNull(userCrud)
    }

    //createUser
    @Test
    fun testCreateUser() {
        Assert.assertEquals(0, userCrud.count())

        val id = userCrud.createUser(USERNAME)

        Assert.assertEquals(1, userCrud.count())
        Assert.assertEquals(id, userCrud.findOne(id).userId)
    }

    @Test
    fun testDelete() {
        Assert.assertEquals(0, userCrud.count())

        val id = userCrud.createUser(USERNAME)
        Assert.assertTrue(userCrud.exists(id))
        Assert.assertTrue(userCrud.findAll().any { n -> n.userId == id })
        Assert.assertEquals(1, userCrud.count())

        userCrud.delete(id)

        Assert.assertFalse(userCrud.exists(id))
        Assert.assertFalse(userCrud.findAll().any { n -> n.userId == id })
        Assert.assertEquals(0, userCrud.count())
    }
}