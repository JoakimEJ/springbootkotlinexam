package pg6100.spring.rest.booksrest.dto

import io.swagger.annotations.ApiModelProperty

data class UserDto(

        @ApiModelProperty("The name of the user")
        var userName: String? = null,

//        @ApiModelProperty("A list of books that this user is selling")
        @ApiModelProperty(hidden=true)
        var sales: MutableList<SalesDto> = mutableListOf(),

        @ApiModelProperty(hidden=true) // TODO: might have to remove this and show model property in swagger ui
        var userId: String? = null
)