package pg6100.spring.rest.booksrest.dto

import pg6100.spring.repositories.sales.SalesEntity

class SalesConverter {

    companion object {

        fun transform(sale: SalesEntity): SalesDto {

            return SalesDto(
                    bookName = sale.book.bookName,
                    salesPrice = sale.salesPrice,
                    bookId = sale.book.bookId,
                    userId = sale.user.userId.toString(),
                    salesId = sale.salesId.toString()
            )
        }

        fun transform(sales: Iterable<SalesEntity>): List<SalesDto> {
            return sales.map { transform(it) }
        }
    }
}