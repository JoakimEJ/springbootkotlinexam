package pg6100.spring.rest.booksrest.dto

import pg6100.spring.repositories.book.BookEntity

class BookConverter {

    companion object {

        fun transform(book: BookEntity): BookDto {

            return BookDto(
                    bookId = book.bookId?.toString(),
                    bookName = book.bookName,
                    sales = book.sales
            )
        }

        fun transform(books: Iterable<BookEntity>): List<BookDto> {
            return books.map { transform(it) }
        }
    }
}