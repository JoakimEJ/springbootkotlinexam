package pg6100.spring.rest.booksrest.dto

import io.swagger.annotations.ApiModelProperty

data class SalesDto(


        @ApiModelProperty("The asking price for the book")
        var salesPrice: Int? = null,

        @ApiModelProperty("Id of the book for sale")
        var bookId: Long? = null,

        @ApiModelProperty("Name of the book for sale")
        var bookName: String? = null,

        @ApiModelProperty("Id of user that is selling the book")
        var userId: String? = null,

        //@JsonIgnore
        @ApiModelProperty(hidden=true) // TODO: might have to remove this and show model property in swagger ui
        var salesId: String? = null
)