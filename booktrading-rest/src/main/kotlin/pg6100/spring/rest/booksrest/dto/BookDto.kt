package pg6100.spring.rest.booksrest.dto

import io.swagger.annotations.ApiModelProperty
import pg6100.spring.repositories.sales.SalesEntity

data class BookDto(


        @ApiModelProperty("The name of the book")
        var bookName: String? = null,

        //@ApiModelProperty("A list of users that have this book for sale")
        @ApiModelProperty(hidden = true) // TODO: hide for now as POST-request is not accepting empty "users" when creating book
        var sales: MutableList<SalesEntity> = mutableListOf(),

        //@JsonIgnore
        @ApiModelProperty(hidden=true) // TODO: might have to remove this and show model property in swagger ui
        var bookId: String? = null
)