package pg6100.spring.rest.booksrest.dto

import pg6100.spring.repositories.user.UserEntity

class UserConverter {

    companion object {

        fun transform(user: UserEntity): UserDto {
            return UserDto(
                    userId = user.userId?.toString(),
                    userName = user.userName,
                    sales = SalesConverter.transform(user.sales).toMutableList()
            )
        }

        fun transform(users: Iterable<UserEntity>): List<UserDto> {
            return users.map { transform(it) }
        }
    }
}