package pg6100.spring.rest.booksrest.api

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pg6100.spring.repositories.book.BookRepository
import pg6100.spring.repositories.sales.SalesRepository
import pg6100.spring.repositories.user.UserRepository
import pg6100.spring.rest.booksrest.dto.*

const val ID_PARAM = "The numeric id of the object"

@Api(value = "/booktrade", description = "Managing entities books, users and sales")
@RequestMapping(
        path = ["/booktrade"],
        produces = [MediaType.APPLICATION_JSON_VALUE]
)
@RestController
//@Validated
class BookTradingRestApi {

    @Autowired
    private lateinit var bookCrud: BookRepository

    @Autowired
    private lateinit var userCrud: UserRepository

    @Autowired
    private lateinit var salesCrud: SalesRepository

    /*
    *   Book related operations
    */

    @ApiOperation("Get all the books")
    @GetMapping(path = ["/books"])
    fun getAllBooks(): ResponseEntity<List<BookDto>> {
        return ResponseEntity.ok(BookConverter.transform(bookCrud.findAll()))
    }

    @ApiOperation("Create a book")
    @PostMapping(path = ["/books"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ApiResponse(code = 201, message = "The id of newly created book")
    fun createBook(
            @ApiParam("Name and price of book. Should not specify id of book")
            @RequestBody
            dto: BookDto)
            : ResponseEntity<Long> {

        if (!(dto.bookId.isNullOrEmpty())) {
            return ResponseEntity.status(400).build()
        }

        if (dto.bookName == null) {
            return ResponseEntity.status(400).build()
        }

        return ResponseEntity.status(201).body(bookCrud.createBook(dto.bookName!!))
    }

    @ApiOperation("Get a single book based on bookId provided in request")
    @GetMapping(path = ["/books/{id}"])
    fun getBook(
            @ApiParam(ID_PARAM)
            @PathVariable("id")
            pathId: String?,
            //
            @ApiParam("Flag to decide if it should load list of sales for this book.")
            @RequestParam("loadSales", required = false)
            loadUsers: Boolean?
    ): ResponseEntity<BookDto> {

        val id: Long
        try {
            id = pathId!!.toLong()
        } catch (e: Exception) {
            return ResponseEntity.status(404).build()
        }

        val dto = bookCrud.findOne(id) ?: return ResponseEntity.status(404).build()

        if (loadUsers == null || loadUsers == false) {
                dto.sales = mutableListOf()
        }

        return ResponseEntity.ok(BookConverter.transform(dto))
    }

    @ApiOperation("Update an existing book")
    @PutMapping(path = ["/books/{id}"], consumes = [(MediaType.APPLICATION_JSON_VALUE)])
    fun updateBook(
            @ApiParam(ID_PARAM)
            @PathVariable("id")
            pathId: Long?,
            //
            @ApiParam("The book that will replace the old one. Cannot change its id though.")
            @RequestBody
            dto: BookDto
    ): ResponseEntity<Any> {

        val bookId = if(pathId.toString().isBlank()) {
            return ResponseEntity.status(404).build()
        } else {
            pathId
        }

        if (!bookCrud.exists(bookId)) {
            return ResponseEntity.status(404).build() // don't create new with PUT
        }

        if (dto.bookName == null) {
            return ResponseEntity.status(400).build()
        }

        bookCrud.update(bookId!!, dto.bookName!!)

        return ResponseEntity.status(204).build()
    }

    @ApiOperation("Delete a book with the given id")
    @DeleteMapping(path = ["/books/{id}"])
    fun deleteBook(@ApiParam(ID_PARAM)
               @PathVariable("id")
               pathId: String?): ResponseEntity<Any> {

        val id: Long
        try {
            id = pathId!!.toLong()
        } catch (e: Exception) {
            return ResponseEntity.status(400).build()
        }

        if (!bookCrud.exists(id)) {
            return ResponseEntity.status(404).build()
        }

        bookCrud.delete(id)
        return ResponseEntity.status(204).build()
    }

    /*
    *   User related operations
    */

    @ApiOperation("Create a user")
    @PostMapping(path = ["/users"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ApiResponse(code = 201, message = "The id of newly created user")
    fun createUser(
            @ApiParam("Name of user. Should not specify id of user")
            @RequestParam("userName", required = true)
            userName: String?
    ): ResponseEntity<Long> {
        return ResponseEntity.status(201).body(userCrud.createUser(userName!!))
    }

    @ApiOperation("Get all the users")
    @GetMapping(path = ["/users"])
    fun getAllUsers(): ResponseEntity<List<UserDto>> {
        return ResponseEntity.ok(UserConverter.transform(userCrud.findAll()))
    }

    @ApiOperation("Get a single user based on userId provided in request")
    @GetMapping(path = ["/users/{id}"])
    fun getUser(
            @ApiParam(ID_PARAM)
            @PathVariable("id")
            pathId: String?
    ): ResponseEntity<UserDto> {

        val id: Long
        try {
            id = pathId!!.toLong()
        } catch (e: Exception) {
            return ResponseEntity.status(404).build()
        }

        return ResponseEntity.ok(UserConverter.transform(userCrud.findOne(id) ?: return ResponseEntity.status(404).build()))
    }

    @ApiOperation("Delete a user with the given id")
    @DeleteMapping(path = ["/users/{id}"])
    fun deleteUser(@ApiParam(ID_PARAM)
                   @PathVariable("id")
                   pathId: String?): ResponseEntity<Any> {

        val id: Long
        try {
            id = pathId!!.toLong()
        } catch (e: Exception) {
            return ResponseEntity.status(400).build()
        }

        if (!userCrud.exists(id)) {
            return ResponseEntity.status(404).build()
        }

        userCrud.delete(id)
        return ResponseEntity.status(204).build()
    }

    /*
    *   Sales related operations
    */

    @ApiOperation("Create a sale")
    @PostMapping(path = ["/sales"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ApiResponse(code = 201, message = "The id of newly created sale")
    fun createSale(
            @ApiParam("Asking price for sale")
            @RequestParam("salesPrice", required = true)
            salesPrice: String?,
            @ApiParam("Id of user selling")
            @RequestParam("userId", required = true)
            userId: String?,
            @ApiParam("Id of book for sale")
            @RequestParam("bookId", required = true)
            bookId: String?
    ): ResponseEntity<Long> {

        val book = bookCrud.findOne(bookId!!.toLong())
        val user = userCrud.findOne(userId!!.toLong())

        return ResponseEntity.status(201).body(salesCrud.createSale(salesPrice!!.toInt(), book!!, user!!))
    }

    @ApiOperation("Get all the sales")
    @GetMapping(path = ["/sales"])
    fun getAllSales(): ResponseEntity<List<SalesDto>> {
        return ResponseEntity.ok(SalesConverter.transform(salesCrud.findAll()))
    }

    @ApiOperation("Delete a sale with the given id")
    @DeleteMapping(path = ["/sales/{id}"])
    fun deleteSale(@ApiParam(ID_PARAM)
                   @PathVariable("id")
                   pathId: String?): ResponseEntity<Any> {

        val id: Long
        try {
            id = pathId!!.toLong()
        } catch (e: Exception) {
            return ResponseEntity.status(400).build()
        }

        if (!salesCrud.exists(id)) {
            return ResponseEntity.status(404).build()
        }

        salesCrud.delete(id)
        return ResponseEntity.status(204).build()
    }

    /*
    //===== Could not get JSON Merge Patch to work, but i am leaving it here to show the attempt. =====//

    @ApiOperation("Modify the sale using JSON Merge Patch")
    @PatchMapping(path = ["/sales/{id}"],
            consumes = ["application/merge-patch+json"])
    fun mergePatchUser(@ApiParam("The id of the sale to patch")
                       @PathVariable("id")
                       id: Long?,
                       @ApiParam("The partial patch")
                       @RequestBody
                       jsonPatch: String
    ): ResponseEntity<Void> {

        val sale = SalesConverter.transform(salesCrud.findOne(id) ?: return ResponseEntity.status(404).build())

        val jackson = ObjectMapper()

        val jsonNode: JsonNode
        try {
            jsonNode = jackson.readValue(jsonPatch, JsonNode::class.java)
        } catch (e: Exception) {
            return ResponseEntity.status(400).build()
        }

        if (jsonNode.has("id")) {
            //shouldn't be allowed to modify the sales id
            return ResponseEntity.status(409).build()
        }


        var newPrice = sale.salesPrice
        var newBookId = sale.bookId
        var newBookName = sale.bookName
        var newUserId = sale.userId

        if (jsonNode.has("salesPrice")) {
            val salesPriceNode = jsonNode.get("salesPrice")
            if (salesPriceNode.isNull) {
                newPrice = null
            } else if (salesPriceNode.isNumber) {
                newPrice = salesPriceNode.asInt()
            } else {
                return ResponseEntity.status(400).build()
            }
        }

        if (jsonNode.has("bookId")) {
            val bookIdNode = jsonNode.get("bookId")
            if (bookIdNode.isNull) {
                newBookId = null
            } else if (bookIdNode.isLong) {
                newBookId = bookIdNode.asLong()
            } else {
                return ResponseEntity.status(400).build()
            }
        }

        if (jsonNode.has("bookName")) {
            val bookNameNode = jsonNode.get("bookName")
            if (bookNameNode.isNull) {
                newBookName = null
            } else if (bookNameNode.isTextual) {
                newBookName = bookNameNode.asText()
            } else {
                return ResponseEntity.status(400).build()
            }
        }

        if (jsonNode.has("userId")) {
            val userIdNode = jsonNode.get("userId")
            if (userIdNode.isNull) {
                newBookName = null
            } else if (userIdNode.isTextual) {
                newBookName = userIdNode.asText()
            } else {
                return ResponseEntity.status(400).build()
            }
        }

        sale.salesPrice = newPrice
        sale.bookId = newBookId
        sale.bookName = newBookName
        sale.userId = newUserId

        return ResponseEntity.status(204).build()
    }
    */
}