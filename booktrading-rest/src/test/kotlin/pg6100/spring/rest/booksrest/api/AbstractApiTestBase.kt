package pg6100.spring.rest.booksrest.api

import io.restassured.RestAssured
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import pg6100.spring.rest.booksrest.BookTradeRestApplication
import pg6100.spring.rest.booksrest.dto.BookDto
import pg6100.spring.rest.booksrest.dto.SalesDto
import pg6100.spring.rest.booksrest.dto.UserDto

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [(BookTradeRestApplication::class)])
abstract class AbstractApiTestBase {

    @LocalServerPort
    protected var port = 0

    val BOOKS_PATH = "/books"
    val USERS_PATH = "/users"
    val SALES_PATH = "/sales"

    @Before
    @After
    fun clean() {

        // http://localhost:8080/booktradingrest/api/booktrade/books
        RestAssured.baseURI = "http://localhost"
        RestAssured.port = port
        RestAssured.basePath = "/booktradingrest/api/booktrade"
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()

        val bookList = given().accept(ContentType.JSON).get(BOOKS_PATH)
                .then()
                .statusCode(200)
                .extract()
                .`as`(Array<BookDto>::class.java)
                .toList()

        bookList.stream().forEach {
            given().pathParam("id", it.bookId)
                    .delete("/books/{id}")
                    .then()
                    .statusCode(204)
        }

        given().get(BOOKS_PATH)
                .then()
                .statusCode(200)
                .body("size()", equalTo(0))

        val userList = given().accept(ContentType.JSON).get(USERS_PATH)
                .then()
                .statusCode(200)
                .extract()
                .`as`(Array<UserDto>::class.java)
                .toList()

        userList.stream().forEach {
            given().pathParam("id", it.userId)
                    .delete("/users/{id}")
                    .then()
                    .statusCode(204)
        }

        given().get(USERS_PATH)
                .then()
                .statusCode(200)
                .body("size()", equalTo(0))

        // should be empty because orphan removal
        given().get("/sales")
                .then()
                .statusCode(200)
                .body("size()", equalTo(0))

    }

    fun createGenericBook(bookName : String): BookDto {

        val book = BookDto(bookName)

        book.bookId = RestAssured.given().contentType(ContentType.JSON)
                .body(book)
                .post(BOOKS_PATH)
                .then()
                .statusCode(201)
                .extract().asString()

        return book
    }

    fun createGenericUser(userName: String): UserDto {

        val user = UserDto(userName)

        user.userId = RestAssured.given().contentType(ContentType.JSON)
                .queryParam("userName", userName)
                .post(USERS_PATH)
                .then()
                .statusCode(201)
                .extract().asString()

        return user
    }

    fun createGenericSale(salesPrice: Int): SalesDto {

        val user = createGenericUser("userA")
        val book = createGenericBook("bookA")

        val sale = SalesDto(12, book.bookId!!.toLong(), book.bookName!!, user.userId!!)
        sale.salesId = RestAssured.given().contentType(ContentType.JSON)
                .queryParam("salesPrice", sale.salesPrice)
                .queryParam("userId", sale.userId)
                .queryParam("bookId", sale.bookId)
                .post(SALES_PATH)
                .then()
                .statusCode(201)
                .extract().asString()

        return sale
    }
}