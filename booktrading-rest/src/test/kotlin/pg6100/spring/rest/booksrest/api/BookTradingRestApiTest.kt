package pg6100.spring.rest.booksrest.api

import io.restassured.RestAssured.*
import io.restassured.http.ContentType
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import pg6100.spring.rest.booksrest.dto.BookDto


class BookTradingRestApiTest : AbstractApiTestBase() {

    @Test
    fun testCleanDB() {

        given().get(BOOKS_PATH).then()
                .statusCode(200)
                .body("size()", equalTo(0))

        given().get(USERS_PATH).then()
                .statusCode(200)
                .body("size()", equalTo(0))

        given().get("/sales").then()
                .statusCode(200)
                .body("size()", equalTo(0))
    }

    @Test
    fun testCreateAndGetBook() {

        given().get(BOOKS_PATH).then().statusCode(200).body("size()", equalTo(0))

        val book = createGenericBook("bookName")

        given().get(BOOKS_PATH).then().statusCode(200).body("size()", equalTo(1))

        given().pathParam("id", book.bookId)//.queryParam("loadUsers", false)
                .get("/books/{id}")
                .then()
                .statusCode(200)
                .body("bookId", equalTo(book.bookId))
                .body("bookName", equalTo(book.bookName))
    }

    @Test
    fun testGetAllBooks() {
        get(BOOKS_PATH).then().body("size()", equalTo(0))
        createGenericBook("bookA")
        createGenericBook("bookB")
        createGenericBook("bookC")
        get(BOOKS_PATH).then().body("size()", equalTo(3))
    }

    @Test
    fun testGetBookWithSalesLoaded() {
        val sale = createGenericSale(12)
        val bookId = sale.bookId

        given().pathParam("id", bookId).queryParam("loadSales", true)
                .get("/books/{id}")
                .then()
                .statusCode(200)
                .body("bookId", equalTo(bookId.toString()))
                .body("sales[0].salesId", equalTo(1)) // cant use sale.salesId here
    }

    @Test
    fun testDeleteBook() {

        val bookId = createGenericBook("bookName").bookId

        get(BOOKS_PATH).then()
                .body("size()", equalTo(1))
                .body("bookId[0]", equalTo(bookId))

        delete("/books/$bookId")

        get(BOOKS_PATH).then().body("bookId", not(containsString(bookId)))
    }

    @Test
    fun testUpdateBook() {

        val bookName = "bookName"

        // Update name (entire object)
        val bookId = createGenericBook(bookName).bookId

        get("books/$bookId").then().body("bookName", equalTo(bookName))

        val updatedBookName = "updatedBookName"

        given().contentType(ContentType.JSON)
                .pathParam("id", bookId)
                .body(BookDto(updatedBookName))
                .put("/books/{id}")
                .then()
                .statusCode(204)

        get("books/$bookId").then().body("bookName", equalTo(updatedBookName))
    }

    @Test
    fun testCreateAndGetUser() {
        given().get(USERS_PATH).then().statusCode(200).body("size()", equalTo(0))

        val user = createGenericUser("userName")

        given().get(USERS_PATH).then().statusCode(200).body("size()", equalTo(1))

        given().pathParam("id", user.userId)
                .get("/users/{id}")
                .then()
                .statusCode(200)
                .body("userId", equalTo(user.userId))
                .body("userName", equalTo(user.userName))
    }

    @Test
    fun testGetAllUsers() {
        get(USERS_PATH).then().body("size()", equalTo(0))
        createGenericUser("userA")
        createGenericUser("userB")
        createGenericUser("userC")
        get(USERS_PATH).then().body("size()", equalTo(3))
    }

    @Test
    fun testDeleteUser() {
        val userId = createGenericUser("userName").userId

        get(USERS_PATH).then()
                .body("size()", equalTo(1))
                .body("userId[0]", equalTo(userId))

        delete("/users/$userId")

        get(USERS_PATH).then().body("userId", not(containsString(userId)))
    }

    @Test
    fun testCreateSale() {
        createGenericSale(12)
    }

    @Test
    fun testGetAllSales() {
        get(SALES_PATH).then().body("size()", equalTo(0))
        createGenericSale(1)
        createGenericSale(2)
        createGenericSale(3)
        get(SALES_PATH).then().body("size()", equalTo(3))
    }

    @Test
    fun testDeleteSale() {
        val saleId = createGenericSale(1).salesId

        get(SALES_PATH).then()
                .body("size()", equalTo(1))
                .body("salesId[0]", equalTo(saleId))

        delete("/sales/$saleId")

        get(SALES_PATH).then().body("salesId", not(containsString(saleId)))

        // verify book and user still exists
        get(USERS_PATH).then().body("size()", equalTo(1))
        get(BOOKS_PATH).then().body("size()", equalTo(1))
    }
}
